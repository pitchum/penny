# -*- coding: utf-8 -*-

import logging ; log = logging.getLogger(__name__)
import pkg_resources
from pyramid.config import Configurator

__version__ = pkg_resources.get_distribution('penny').version


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    settings['app_version'] = __version__
    config = Configurator(settings=settings)
    config.include('pyramid_chameleon')
    
    init_dirs(config)
    
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    config.add_route('compute', '/compute')
    config.add_route('sess', '/s/{session}')
    config.add_route('sess.new', '/s/')
    config.scan()
    return config.make_wsgi_app()


def init_dirs(config):
    '''Creates missing folders required for the webapp.'''
    import os
    parent_dir = config.registry.settings.get('storage.dir')
    if not os.path.exists(parent_dir):
        os.makedirs(parent_dir)
        log.debug("Folder created: %s" % parent_dir)

    default_git_author = 'penny <penny@localhost>'
    from dulwich.repo import check_user_identity, InvalidUserIdentity
    try:
        user_defined_git_author = config.registry.settings.get('git.author', default_git_author)
        check_user_identity(user_defined_git_author.encode())
    except InvalidUserIdentity:
        log.warning("Ignoring invalid config parameter for git.author: '{}'. Using default value '{}'".format(user_defined_git_author, default_git_author))
        config.registry.settings['git.author'] = default_git_author

