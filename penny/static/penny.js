function render() {
	fetch("../compute", { // XXX hard-coded URL
			method: "POST",
			headers: new Headers({"X-Requested-With": "XMLHttpRequest",}),
			body: document.querySelector("pre#source").innerText
		}
	).then(function(response) {
		return response.json();
	}).then(function (blob) {
		document.querySelector("#renderer").textContent = blob.balance;
	});
}
