import logging ; logging.basicConfig(level=logging.INFO) ; log = logging.getLogger(__name__)
from .lib import BalanceComputer

def quickbalance():
    import sys
    if '-v' in sys.argv or True: ## TODO use argparse instead
        logging.getLogger('penny.lib').setLevel(logging.DEBUG)
    if len(sys.argv) == 1:
        csvdata = """
payeur    ; montant ; bénéficiaire(s) ; "commentaire"
michel    ;   122 ; groupe ; "acompte hébergement"
herbert   ;   122 ; groupe ; "acompte hébergement"
leonard   ;   100 ; groupe ; "covoiturage (essence + péage)"
herbert   ;   100 ; groupe ; "covoiturage (essence + péage)"
maxime    ;    25 ; michel ; "hébergement"
leonard   ;    25 ; michel ; "hbergement"
"""
    else:
        inputurl = sys.argv[1]
        if inputurl.startswith('http'):
            import requests
            csvdata = fetch_csv_from_pad(inputurl)
        else:
            with open(sys.argv[1], 'r') as csvfile:
                csvdata = csvfile.read()
    
    bc = BalanceComputer()
    balance = bc.parse_and_compute(csvdata)
    print(balance)
