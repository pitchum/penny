# -*- coding: utf-8 -*-

import logging ; log = logging.getLogger(__name__)
from collections import namedtuple, OrderedDict
Transaction = namedtuple('Transaction', ['payer', 'amount', 'recipient', 'comment'])

class BalanceComputer(object):
    
    def __init__(self):
        self.group = set()
        self.balance = OrderedDict() # holds the final results
    
    
    def parse_and_compute(self, csvdata):
        res = ""
        # transaction that needs a second pass after group members are identified
        group_txs = []
        for tx in self.iter_lines(csvdata):
            if tx is None:
                continue
            self.group.add(tx.payer)
            if 'group' in tx.recipient:
                # TODO remember row for a second pass
                group_txs.append(tx)
            elif ',' in tx.recipient:
                self.compute_transaction(tx)
            else:
                self.group.add(tx.recipient)
                self.compute_transaction(tx)
        # Now that all group members are known, we can compute transactions
        # involving group in a second pass.
        for tx in group_txs:
            self.compute_transaction(tx)

        res += "\n"
        res += "    Balance :\n"
        res += "    \n"
        import operator
        sorted_balance = OrderedDict(sorted(self.balance.items(), key=operator.itemgetter(1)))
        for person in sorted_balance:
            individual_balance = self.balance[person]
            if individual_balance < -1:
                res += "      %s doit récupérer %0.0f€.\n" % (person, -individual_balance)
            elif individual_balance < 1:
                pass
            else:
                res += "      %s doit rembourser %0.0f€.\n" % (person, individual_balance)
        res += "\n"
        return res


    def compute_transaction(self, tx, recipient=None):
        if 'group' in tx.recipient:
            subamount = tx.amount / len(self.group)
            log.debug("%s paye %0.2f€ pour %d personnes" % (tx.payer, tx.amount, len(self.group)))
            self.update_balance(tx.payer, -tx.amount)
            for member in self.group:
                member = member.strip()
                self.update_balance(member, subamount)
                if tx.payer != member:
                   log.debug("  %0.2f€ pour %s" % (subamount, member))
        elif ',' in tx.recipient: # multiple recipients
            recipients = tx.recipient.split(',')
            subamount = tx.amount / len(recipients)
            log.debug("%s paye %0.2f€ pour %d personnes" % (tx.payer, tx.amount, len(recipients)))
            self.update_balance(tx.payer, -tx.amount)
            for member in recipients:
                member = member.strip()
                self.group.add(member)
                self.update_balance(member, subamount)
                if tx.payer != member:
                    log.debug("  %0.2f€ pour %s" % (subamount, member))
        else:
            log.debug("%s paye %0.2f€ pour %s" % (tx.payer, tx.amount, tx.recipient))
            self.update_balance(tx.recipient, tx.amount)
            self.update_balance(tx.payer, -tx.amount)


    def update_balance(self, person, amount):
        if person not in self.balance:
            self.balance[person] = 0
        self.balance[person] = self.balance[person] + amount


    def iter_lines(self, csvdata):
        import csv
        lines = csvdata.strip().split("\n")
        r = csv.reader(lines, delimiter=";", quotechar='"')
        firstrow = next(r)
        def line2transaction(row):
            return Transaction(
                row[0].strip(),
                float(row[1].strip()),
                row[2].strip(),
                row[3].strip(),
            )
        if 'payeur' not in firstrow[0]:
            yield(line2transaction(firstrow))
        for row in r:
            try:
                yield(line2transaction(row))
            except:
                log.debug("Ignored unparseable line: '%s'" % row, exc_info=True)
