# -*- coding: utf-8 -*-

import logging ; log = logging.getLogger(__name__)
from pyramid.view import view_config
import os
from .lib import BalanceComputer

from dulwich.repo import Repo
from dulwich import porcelain


def _storage_dir(request):
    return request.registry.settings.get('storage.dir')

def _git_author(request):
    return request.registry.settings.get('git.author', 'penny <penny@localhost>')


@view_config(route_name='home', renderer='templates/sessions.pt')
def home(request):
    session_names = (os.path.splitext(f)[0] for f in os.listdir(_storage_dir(request)))
    return {'sessions': session_names}


@view_config(route_name='sess', renderer='templates/summary.pt', xhr=False, request_method='GET')
def summary(request):
    sessionname = request.matchdict.get('session')
    fname = os.path.join(_storage_dir(request), sessionname + '.csv')
    csvdata = ""
    with open(fname, 'r') as f:
        csvdata = f.read()
    return {
        "sessionname": sessionname,
        "source": csvdata,
    }


@view_config(route_name='sess', renderer='json', xhr=True, request_method='GET')
def session_source(request):
    sessionname = request.matchdict.get('session')
    fname = os.path.join(_storage_dir(request), sessionname + '.csv')
    csvdata = ""
    with open(fname, 'r') as f:
        csvdata = f.read()
    return {
        "source": csvdata,
    }


@view_config(route_name='sess.new', renderer='json', xhr=True, request_method='PUT')
def session_create(request):
    requested_name = request.text
    if not requested_name or len(requested_name) < 2:
        request.response.status = 400
        return _error_("Nom invalide: %s" % requested_name)
    fname = os.path.join(_storage_dir(request), str(requested_name) + '.csv')
    if os.path.isfile(fname):
        request.response.status = 400
        return _error_("Nom déjà utilisé: %s" % requested_name)
    log.debug("Creating file {0}".format(str(fname)))
    with open(fname, 'w') as f:
        f.write('payeur ; montant ; bénéficiaire(s) ; "commentaire"\n')
        f.write('''alice ; 42 ; bob ; "ceci est une transaction d'exemple, supprimez cette ligne"\n''')
    return {"status": "succes"}


@view_config(route_name='sess', renderer='json', xhr=True, request_method='POST')
def session_update(request):
    sessionname = request.matchdict.get('session')
    fname = os.path.join(_storage_dir(request), sessionname + '.csv')
    if not os.path.isfile(fname):
        return _error_("Cette session penny n'existe pas : {0}".format(sessionname))
    csvdata = request.text
    # TODO perform some sanity checks here
    with open(fname, 'w') as f:
        f.write(csvdata)
    _git_commit(os.path.join(_storage_dir(request)), _git_author(request).encode())
    return {"status": "success"}


@view_config(route_name='compute', renderer='json', xhr=True, request_method='POST')
def compute(request):
    bc = BalanceComputer()
    balance = bc.parse_and_compute(request.text)
    return {
        "balance": balance,
    }


def _error_(msg):
    return {
        "status": "error",
        "error": msg,
    }

def _git_commit(repodir, git_author):
    # Init a git repo if absent
    if not os.path.isdir(os.path.join(repodir, ".git")):
        log.info("Initializing git repository '{}' with author '{}'".format(os.path.abspath(repodir), git_author.decode()))
        repo = Repo.init(repodir)
        repo.do_commit(message=b"Initial commit",
            committer=git_author,
            author=git_author)
    repo = Repo(repodir)
    st = porcelain.status(repo.path)
    if st.unstaged:
        repo.stage(st.unstaged)
        repo.do_commit(
            message=b"Auto-update",
            author=git_author)
    elif st.untracked:
        repo.stage(st.untracked)
        repo.do_commit(
            message=b"Auto-add",
            author=git_author)
