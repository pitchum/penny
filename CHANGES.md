# Changelog

## Unreleasead

- Improve responsiveness
- Auto-scroll to bottom on page load


## 0.2 - 2017-07-25

- Bugfix: bad balance computation when payer is also part of the recipients
- Versionning user changes with git (using dulwich)



## 0.1 - 2017-07-25

- Initial version
- WebUI + CLI

