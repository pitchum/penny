*a debt paid is a friend kept*

Penny is a simple webapp to compute how much money you owe or how much
money you should get back at the end of the holiday. All you need to do
during the holiday is to report each money transaction that occurs and
penny will compute the balance.

Penny is written in python using [the pyramid framework](http://docs.pylonsproject.org/projects/pyramid/en/latest/).
No database is needed, all data is stored in flat files.


## Quick start guide

    python setup.py develop --user
    pserve development.ini
    # now open your browser at http://localhost:6543/


## Example

Alice, Bob and Mallory spent a week on holiday :

    alice  ;  125 ; group ; "restaurant the first day"
    bob    ;  267 ; group ; "shopping"
    alice  ;   20 ; alice, bob ; "ice creams on the beach"
    mallory;    0 ; group ; "this line ensures mallory is considered"

This yields the following balance:

      bob doit récupérer 126€.
      alice doit récupérer 4€.
      mallory doit rembourser 131€.

